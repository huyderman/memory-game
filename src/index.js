import './style.css'
import CreditsComponent from './components/CreditsComponent'
import GameState from './GameState'
import GameBoardComponent from './components/GameBoardComponent'

const state = new GameState({})

const gameBoard = new GameBoardComponent({state})

document.body.appendChild(gameBoard.render())
document.body.appendChild((new CreditsComponent()).render())
