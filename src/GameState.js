import shuffle from './func/shuffle'
import Card from './Card'

export default class GameState {
  get moves () {
    return this._moves
  }

  get cards () {
    return this._cards
  }

  constructor ({numCardPairs = 6}) {
    let cards = []
    for (let i = 0; i < numCardPairs; i++) {
      cards.push(
        new Card({symbol: i}),
        new Card({symbol: i})
      )
    }

    this._cards = shuffle(cards)
    this._firstCard = -1
    this._secondCard = -1
    this._moves = 0
  }

  flipCard (id) {
    if (this.cards[id].flipped) {
      // TODO Illegal move feedback
      return
    }

    if (this._firstCard === -1) {
      this._firstCard = id
      this.cards[id] = this.cards[id].flip()
    } else {
      this._secondCard = id
      this.cards[id] = this.cards[id].flip()
    }
  }

  validateCards () {
    if (this._secondCard === -1) { return false }

    let firstCard = this.cards[this._firstCard]
    let secondCard = this.cards[this._secondCard]
    if (firstCard.symbol === secondCard.symbol) {
      // Match
      // TODO increase score, etc.
    } else {
      // Not-Match
      this.cards[this._firstCard] = firstCard.flip()
      this.cards[this._secondCard] = secondCard.flip()
      // TODO more feedback
    }

    this._firstCard = -1
    this._secondCard = -1
    this._moves++

    if (this.cards.every(card => card.flipped)) {
      alert(`Congratulations!\nYou've matched all the cards in ${this.moves} moves.\nReload the page to play again.`)
    }
  }
}
