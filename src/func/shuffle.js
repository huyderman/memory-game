function getRandomIndex (max) {
  return Math.floor(Math.random() * (max + 1))
}

// Fisher-Yates inspired shuffle
export default function shuffle (arr) {
  let input = arr.slice()
  let output = []

  for (let i = input.length - 1; i >= 0; i--) {
    const randomIndex = getRandomIndex(i)
    const randomVal = input[randomIndex]

    input[randomIndex] = input[i]
    output.push(randomVal)
  }

  return output
}
