export default class Card {
  get flipped () {
    return this._flipped
  }

  get symbol () {
    return this._symbol
  }

  constructor ({symbol, flipped}) {
    this._symbol = symbol
    this._flipped = flipped
  }

  flip () {
    return new Card({symbol: this.symbol, flipped: !this.flipped})
  }
}
