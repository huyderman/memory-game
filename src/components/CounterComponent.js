export default class CounterComponent {
  get state () {
    return this._state
  }
  get parent () {
    return this._parent
  }
  constructor ({state, parent}) {
    this._state = state
    this._parent = parent
  }

  render () {
    if (!this._element) {
      this._element = document.createElement('div')
      this._element.classList.add('counter')
    }
    this._element.innerText = `Total Moves: ${this.state.moves}`

    return this._element;
  }
}
