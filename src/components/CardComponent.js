const SYMBOLS = [
  require('../images/island.svg'),
  require('../images/locked-chest.svg'),
  require('../images/monkey.svg'),
  require('../images/open-treasure-chest.svg'),
  require('../images/pirate-captain.svg'),
  require('../images/pirate-flag.svg')
]

export default class CardComponent {
  get parent () {
    return this._parent
  }

  get cardId () {
    return this._cardId
  }

  get card () {
    return this._state.cards[this.cardId]
  }

  constructor ({cardId, state, parent}) {
    this._cardId = cardId
    this._state = state
    this._parent = parent
  }

  render () {
    if (!this._element) { this._element = this.createElement() }

    if (this.card.flipped === undefined) {
      // Don't apply style until explicitly flipped
    } else if (this.card.flipped) {
      this._element.classList.add('card-face-up')
      this._element.classList.remove('card-face-down')
    } else {
      this._element.classList.add('card-face-down')
      this._element.classList.remove('card-face-up')
    }

    return this._element
  }

  createElement () {
    let element = document.createElement('div')
    element.classList.add('card')
    element.id = `game-card-${this.cardId}`
    element.appendChild(this.createImage())

    element.onclick = () => {
      this._state.flipCard(this.cardId)
      this.render()

      // Set delay to ensure player can see flipped cards
      setTimeout(()=> {
        this._state.validateCards()
        this.parent.render()
      }, 10)
    }

    return element
  }

  createImage () {
    const image = new Image()
    image.src = SYMBOLS[this.card.symbol]
    image.classList.add('card-symbol')

    return image
  }
}
