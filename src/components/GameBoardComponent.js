import CardComponent from './CardComponent'
import CounterComponent from './CounterComponent'

export default class GameBoardComponent {
  get state () {
    return this._state
  }

  constructor ({state}) {
    this._state = state
    this._cardComponents = state.cards.map((_card, cardId) => {
        return new CardComponent({parent: this, state, cardId})
      }
    )
    this._counterComponent = new CounterComponent({parent: this, state})
  }

  render () {
    if (!this._element) {
      this._element = this.createElement()
      return this._element
    }
    this._cardComponents.forEach((component) => component.render())
    this._counterComponent.render()

    return this._element
  }

  createElement () {
    let element = document.createElement('div')
    element.classList.add('game-board')
    this._cardComponents.forEach((component) => element.appendChild(component.render()))
    element.appendChild(this._counterComponent.render())

    return element
  }
}
