export default class CreditsComponent {
  render() {
    if (this._element) { return this._element }
    let element = document.createElement('section')

    element.classList.add('credits-section')
    element.innerHTML = require('../credits.md')

    this._element = element
    return element
  }
}
