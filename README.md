# Memory Game

A simple implementation of the Memory Game in VanillaJS.

The game is implemented with a distinct game state, and the rendering is done by separate components which renders
different parts of the interface by creating DOM elements.

## Running the game

Install dependencies with:

    yarn install

Launch the dev server with:

    yarn run start  
    
## How to play

Click on cards, trying to make pairs. If you flip all cards, you win.

## Possible Future improvements

* Implement a proper win alert
* Implement resetting game state
* Difficulty setting (number of cards)
* Better graphics
    - image background for cards/playing board
    - use CSS 3D transformations for "card-flipping effect"
    - better visual cues for selected card, invalid pair, etc.
* Scale gameboard based on screen-size/orientation
* Sound effects — card flipping sound, buzzer on bad combo, fanfare on win, etc.
* Prevent easy cheating by "dragging" cards
